﻿using System;

namespace exercise_26
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            var student = new Student();

            student.Name1 = "Vincent Ning";
            student.ID = 10010690 ;

            Console.WriteLine(student.Print());
            
            
            
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
    class Student
    {
        string Name;
        int IDNumber;

        public Student(string name,int idNumber)
        {
            Name = name;
            IDNumber = idNumber;
        }

        public Student()
        {

        }

        public string Name1
        {
            get { return Name; }
            set { Name = value ; }
        }
        public int ID
        {
            get { return IDNumber; }
            set { IDNumber = value; }
        }
        public string Print()
        {
            return $"This student's name is {Name},The ID number is {IDNumber}";
        }


    }
}

